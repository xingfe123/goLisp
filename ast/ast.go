package ast

import (
	"fmt"
	"reflect"
	"strconv"
	"xingfe91/LispEx/binder"
	"xingfe91/LispEx/constants"
	"xingfe91/LispEx/converter"
	"xingfe91/LispEx/scope"
	"xingfe91/LispEx/value"
)

type Set struct {
	Pattern *Name
	Value   Node
}

func NewSet(pattern *Name, val Node) *Set {
	return &Set{Pattern: pattern, Value: val}
}

func (self *Set) Eval(env *scope.Scope) value.Value {
	val := self.Value.Eval(env)
	binder.Assign(env, self.Pattern.Identifier, val)
	return nil
}

func (self *Set) String() string {
	return fmt.Sprintf("(%s %s %s)", constants.SET, self.Pattern, self.Value)
}

type Select struct {
	Clauses [][]Node
}

func NewSelect(clauses [][]Node) *Select {
	return &Select{Clauses: clauses}
}

func (self *Select) Eval(env *scope.Scope) value.Value {
	cases := make([]reflect.SelectCase, len(self.Clauses))
	for i, clause := range self.Clauses {
		// parser guarantee the test case is a Call or Name
		//   Call.Callee is either `<-chan' or `chan<-'
		//   Name.Identifier must be `default'
		var test *Call
		var name *Name
		var args []value.Value
		var channel *value.Channel

		switch clause[0].(type) {
		case *Call:
			test, _ = clause[0].(*Call)
			name, _ = test.Callee.(*Name)
			args = EvalList(test.Args, env)
			_, ok := args[0].(*value.Channel)
			if ok {
				channel, _ = args[0].(*value.Channel)
			} else {
				panic(fmt.Sprintf("incorrect argument type for `%s', expected: channel, given: %s", name, args[0]))
			}
		case *Name:
			name, _ = clause[0].(*Name)
		}

		if name.Identifier == constants.CHAN_SEND {
			// send to chan `chan<-'
			cases[i].Send = reflect.ValueOf(args[1])
			cases[i].Dir = reflect.SelectSend
			cases[i].Chan = reflect.ValueOf(channel.Value)
		} else if name.Identifier == constants.CHAN_RECV {
			// receive from chan `<-chan'
			cases[i].Dir = reflect.SelectRecv
			cases[i].Chan = reflect.ValueOf(channel.Value)
		} else if name.Identifier == constants.DEFAULT {
			// default
			cases[i].Dir = reflect.SelectDefault
		}
	}

	chosen, recv, ok := reflect.Select(cases)
	exprs := self.Clauses[chosen]

	if len(exprs) == 1 {
		if ok {
			return recv.Interface().(value.Value)
		} else {
			return nil
		}
	} else {
		exprs = exprs[1:]
		for i := 0; i < len(exprs)-1; i++ {
			exprs[i].Eval(env)
		}
		return exprs[len(exprs)-1].Eval(env)
	}
}

func (self *Select) String() string {
	var result string
	for _, clause := range self.Clauses {
		var s string
		for i, expr := range clause {
			if i == 0 {
				s += fmt.Sprint(expr)
			} else {
				s += fmt.Sprintf(" %s", expr)
			}
		}
		result += fmt.Sprintf(" (%s)", s)
	}
	return fmt.Sprintf("(select %s)", result)
}

//////////////////////////////////////////////////////
type Pair struct {
	First  Node
	Second Node
}

/////////////////////////////////////////////////////////////////////////////
var NilPair = NewEmptyPair()

type EmptyPair struct {
}

func NewEmptyPair() *EmptyPair {
	return &EmptyPair{}
}

func (self *EmptyPair) Eval(s *scope.Scope) value.Value {
	return value.NilPairValue
}

func (self *EmptyPair) String() string {
	return "()"
}

//////////////////////////////////////////////////////////////////////////////
func NewPair(first, second Node) *Pair {
	if second == nil {
		second = NilPair
	}
	return &Pair{First: first, Second: second}
}

func (self *Pair) Eval(env *scope.Scope) value.Value {
	var first value.Value
	var second value.Value

	if self.Second == NilPair {
		second = value.NilPairValue
	} else {
		switch self.Second.(type) {
		case *Name:
			second = value.NewSymbol(self.Second.(*Name).Identifier)
		default:
			second = self.Second.Eval(env)
		}
	}

	if name, ok := self.First.(*Name); ok {
		// treat Name as Symbol
		first = value.NewSymbol(name.Identifier)
	} else if _, ok := self.First.(*UnquoteBase); ok {
		// our parser garantees unquote-splicing only appears in quasiquote
		// and unquote-splicing will be evaluated to a list
		first = self.First.Eval(env)
		// () empty list must be handled
		if first == value.NilPairValue {
			return second
		}
		// seek for the last element
		var last value.Value = first
		for {
			switch last.(type) {
			case *value.PairValue:
				pair := last.(*value.PairValue)
				if pair.Second == value.NilPairValue {
					pair.Second = second
					return first
				}
				last = pair.Second
			default:
				if second == value.NilPairValue {
					return first
				} else {
					// `(,@(cdr '(1 . 2) 3))
					panic(fmt.Sprintf("unquote-splicing: expected list?, given: %s", first))
				}
			}
		}
	} else {
		first = self.First.Eval(env)
	}
	return value.NewPairValue(first, second)
}

func (self *Pair) String() string {
	if self.Second == NilPair {
		return fmt.Sprintf("(%s)", self.First)
	}
	switch self.Second.(type) {
	case *Pair:
		return fmt.Sprintf("(%s %s", self.First, self.Second.String()[1:])
	default:
		return fmt.Sprintf("(%s . %s)", self.First, self.Second)
	}
}

type Node interface {
	value.Value
	Eval(s *scope.Scope) value.Value
}

func EvalList(nodes []Node, s *scope.Scope) []value.Value {
	values := make([]value.Value, 0, len(nodes))
	for _, node := range nodes {
		values = append(values, node.Eval(s))
	}
	return values
}

type Name struct {
	Identifier string
}

func NewName(identifier string) *Name {
	return &Name{Identifier: identifier}
}

func (self *Name) Eval(env *scope.Scope) value.Value {
	if val := env.Lookup(self.Identifier); val != nil {
		return val.(value.Value)
	} else {
		panic(fmt.Sprintf("%s: undefined identifier", self.Identifier))
	}
}

func (self *Name) String() string {
	return self.Identifier
}

type LetStar struct {
	Patterns []*Name
	Exprs    []Node
	Body     Node
}

func NewLetStar(patterns []*Name, exprs []Node, body Node) *LetStar {
	return &LetStar{Patterns: patterns, Exprs: exprs, Body: body}
}

func (self *LetStar) Eval(env *scope.Scope) value.Value {
	// Let* is similar to let, but the bindings are performed sequentially
	// from left to right, and the region of a binding indicated by
	// (<variable> <init>) is that part of the let* expression to the right
	// of the binding. Thus the second binding is done in an environment in
	// which the first binding is visible, and so on.

	for i := 0; i < len(self.Patterns); i++ {
		env = scope.NewScope(env)
		binder.Define(env, self.Patterns[i].Identifier, self.Exprs[i].Eval(env))
	}
	return self.Body.Eval(env)
}

func (self *LetStar) String() string {
	var bindings string
	for i := 0; i < len(self.Patterns); i++ {
		if i == 0 {
			bindings += fmt.Sprintf("(%s %s)", self.Patterns[i], self.Exprs[i])
		} else {
			bindings += fmt.Sprintf(" (%s %s)", self.Patterns[i], self.Exprs[i])
		}
	}
	return fmt.Sprintf("(let* (%s) %s)", bindings, self.Body)
}

type LetRec struct {
	Patterns []*Name
	Exprs    []Node
	Body     Node
}

func NewLetRec(patterns []*Name, exprs []Node, body Node) *LetRec {
	return &LetRec{Patterns: patterns, Exprs: exprs, Body: body}
}

func (self *LetRec) Eval(s *scope.Scope) value.Value {
	// The <variable>s are bound to fresh locations holding undefined values,
	// the <init>s are evaluated in the resulting environment
	// (in some unspecified order), each <variable> is assigned to the result of
	// the corresponding <init>, the <body> is evaluated in the resulting
	// environment, and the value(s) of the last expression in <body> is(are)
	// returned. Each binding of a <variable> has the entire letrec expression
	// as its region, making it possible to define mutually recursive procedures.

	env := scope.NewScope(s)
	extended := make([]*scope.Scope, len(self.Patterns))
	for i := 0; i < len(self.Patterns); i++ {
		extended[i] = scope.NewScope(env)
		binder.Define(extended[i], self.Patterns[i].Identifier, self.Exprs[i].Eval(env))
	}
	for i := 0; i < len(extended); i++ {
		env.PutAll(extended[i])
	}
	return self.Body.Eval(env)
}

func (self *LetRec) String() string {
	var bindings string
	for i := 0; i < len(self.Patterns); i++ {
		if i == 0 {
			bindings += fmt.Sprintf("(%s %s)", self.Patterns[i], self.Exprs[i])
		} else {
			bindings += fmt.Sprintf(" (%s %s)", self.Patterns[i], self.Exprs[i])
		}
	}
	return fmt.Sprintf("(letrec (%s) %s)", bindings, self.Body)
}

type Let struct {
	Patterns []*Name
	Exprs    []Node
	Body     Node
}

func NewLet(patterns []*Name, exprs []Node, body Node) *Let {
	return &Let{Patterns: patterns, Exprs: exprs, Body: body}
}

func (self *Let) Eval(s *scope.Scope) value.Value {
	// The <init>s are evaluated in the current environment
	// (in some unspecified order), the <variable>s are bound
	// to fresh locations holding the results, the <body> is
	// evaluated in the extended environment, and the value(s)
	// of the last expression of <body> is(are) returned.

	env := scope.NewScope(s)
	extended := scope.NewScope(s)
	for i := 0; i < len(self.Patterns); i++ {
		binder.Define(extended, self.Patterns[i].Identifier, self.Exprs[i].Eval(env))
	}
	return self.Body.Eval(extended)
}

func (self *Let) String() string {
	var bindings string
	for i := 0; i < len(self.Patterns); i++ {
		if i == 0 {
			bindings += fmt.Sprintf("(%s %s)", self.Patterns[i], self.Exprs[i])
		} else {
			bindings += fmt.Sprintf(" (%s %s)", self.Patterns[i], self.Exprs[i])
		}
	}
	return fmt.Sprintf("(let (%s) %s)", bindings, self.Body)
}

/////////////////////////////////////
type Lambda struct {
	Params Node
	Body   Node
}

func NewLambda(params Node, body Node) *Lambda {
	if params == nil {
		params = NilPair
	}
	return &Lambda{Params: params, Body: body}
}

func (self *Lambda) Eval(env *scope.Scope) value.Value {
	return value.NewClosure(env, self)
}

func (self *Lambda) String() string {
	return fmt.Sprintf("(lambda %s %s)", self.Params, self.Body)
}

///////////////////
type If struct {
	Test Node
	Then Node
	Else Node
}

func NewIf(test, then, else_ Node) *If {
	return &If{Test: test, Then: then, Else: else_}
}

func (self *If) Eval(env *scope.Scope) value.Value {
	tv := self.Test.Eval(env)
	if bv, ok := tv.(*value.BoolValue); ok {
		if bv.Value == false {
			if self.Else == nil {
				return nil
			} else {
				return self.Else.Eval(env)
			}
		}
	}
	return self.Then.Eval(env)
}

func (self *If) String() string {
	return fmt.Sprintf("(%s %s %s %s)",
		constants.IF, self.Test, self.Then, self.Else)
}

////////////////////////////////////////////////////////
type Go struct {
	Expr Node
}

func NewGo(expr Node) *Go {
	return &Go{Expr: expr}
}

func (self *Go) Eval(env *scope.Scope) value.Value {
	// We need to recover the panic message of goroutine
	go func() {
		defer func() {
			if err := recover(); err != nil {
				fmt.Println(err)
			}
		}()
		self.Expr.Eval(scope.NewScope(env))
	}()
	return nil
}

func (self *Go) String() string {
	return fmt.Sprintf("(go %s)", self.Expr)
}

///////////////////////////////////////////////////////////
type Function struct {
	Caller *Name
	Body   Node
}

func NewFunction(caller *Name, body Node) *Function {
	return &Function{Caller: caller, Body: body}
}

func (self *Function) Eval(env *scope.Scope) value.Value {
	return self.Body.Eval(env)
}

func (self *Function) String() string {
	return fmt.Sprintf("%s", self.Body)
}

/////////////////////////////////////////
type Force struct {
	Promise Node
}

func NewForce(promise Node) *Force {
	return &Force{Promise: promise}
}

func (self *Force) Eval(s *scope.Scope) value.Value {
	val := self.Promise.Eval(s)
	if promise, ok := val.(*value.Promise); ok {
		if promise.IsVal == false {
			return nil
		} else {
			promise.IsVal = false
			env := promise.Env.(*scope.Scope)
			lazy := promise.Lazy.(Node)
			return lazy.Eval(env)
		}
	} else {
		panic(fmt.Sprintf("force: expected argument of type <promise>, given: %s", val))
	}
}

func (self *Force) String() string {
	return fmt.Sprintf("(force %s)", self.Promise)
}

////////////////////////////////////////////////////////////////////
type Begin struct {
	Body Node
}
type Call struct {
	Callee Node
	Args   []Node
}

func NewCall(callee Node, args []Node) *Call {
	return &Call{Callee: callee, Args: args}
}

func NewBegin(body Node) *Begin {
	return &Begin{Body: body}
}

type Define struct {
	Pattern *Name
	Value   Node
}

func NewDefine(pattern *Name, val Node) *Define {
	return &Define{Pattern: pattern, Value: val}
}

func (self *Define) Eval(env *scope.Scope) value.Value {
	binder.Define(env, self.Pattern.Identifier, self.Value.Eval(env))
	return nil
}

func (self *Define) String() string {
	return fmt.Sprintf("(define %s %s)", self.Pattern, self.Value)
}

type Delay struct {
	Expr Node
}

func NewDelay(expr Node) *Delay {
	return &Delay{Expr: expr}
}

func (self *Delay) Eval(env *scope.Scope) value.Value {
	return value.NewPromise(env, self.Expr)
}

func (self *Delay) String() string {
	return fmt.Sprintf("(delay %s)", self.Expr)
}

/////////////////////////////////////////////////////////////
type Float struct {
	Value float64
}

func NewFloat(s string) *Float {
	val, err := strconv.ParseFloat(s, 64)
	if err != nil {
		panic(fmt.Sprintf("%s is not float format", s))
	}
	return &Float{Value: val}
}

func (self *Float) Eval(s *scope.Scope) value.Value {
	return value.NewFloatValue(self.Value)
}

func (self *Float) String() string {
	return strconv.FormatFloat(self.Value, 'f', -1, 64)
}

////////////////////////////////////////////////////////////////

func (self *Call) Eval(s *scope.Scope) value.Value {
	callee := self.Callee.Eval(s)
	// we will handle (+ . (1)) latter
	args := EvalList(self.Args, s)

	switch callee.(type) {
	case *value.Closure:
		curry := callee.(*value.Closure)
		env := scope.NewScope(curry.Env.(*scope.Scope))
		lambda, ok := curry.Body.(*Lambda)
		if !ok {
			panic(fmt.Sprint("unexpected type: ", curry.Body))
		}
		// bind call arguments to parameters
		// these nodes should be in Lisp pair structure
		BindArguments(env, lambda.Params, converter.SliceToPairValues(args))
		return lambda.Body.Eval(env)
	case value.PrimFunc:
		return callee.(value.PrimFunc).Apply(args)
	default:
		panic(fmt.Sprintf("%s: not allowed in a call context, args: %s", callee, self.Args[0]))
	}
}

func (self *Call) String() string {
	var s string
	for _, arg := range self.Args {
		s += fmt.Sprintf(" %s", arg)
	}
	return fmt.Sprintf("(%s%s)", self.Callee, s)
}

func (self *Begin) Eval(env *scope.Scope) value.Value {
	return self.Body.Eval(env)
}

func (self *Begin) String() string {
	if self.Body.String() == "" {
		return "(begin)"
	} else {
		return fmt.Sprintf("(begin %s)", self.Body)
	}
}

//////////////////////////////////////////////

func BindArguments(env *scope.Scope, params Node, args value.Value) {
	if name, ok := params.(*Name); ok && args == value.NilPairValue {
		// ((lambda x <body>) '())
		env.Put(name.Identifier, args)
		return
	}
	for {
		if params == NilPair && args == value.NilPairValue {
			return
		} else if params == NilPair && args != value.NilPairValue {
			panic(fmt.Sprint("too many arguments"))
		} else if params != NilPair && args == value.NilPairValue {
			panic(fmt.Sprint("missing arguments"))
		}
		switch params.(type) {
		case *Pair:
			// R5RS declare first element must be a *Name*
			name, _ := params.(*Pair).First.(*Name)
			pair, ok := args.(*value.PairValue)
			if !ok {
				panic(fmt.Sprint("arguments does not match given number"))
			}
			env.Put(name.Identifier, pair.First)
			params = params.(*Pair).Second
			args = pair.Second
		case *Name:
			env.Put(params.(*Name).Identifier, args)
			return
		}
	}
}
