package ast

import (
	"fmt"
	"strconv"
	"strings"
	"xingfe91/LispEx/scope"
	"xingfe91/LispEx/value"
)

type String struct {
	Value string
}

func NewString(val string) *String {
	return &String{Value: val[1 : len(val)-1]}
}

func (self *String) Eval(env *scope.Scope) value.Value {
	return value.NewStringValue(self.Value)
}

func (self *String) String() string {
	return fmt.Sprintf("\"%s\"", self.Value)
}

////////////////////////////////////////////////////////////
///////////////////////////////////////////
type Int struct {
	Value int64
	Base  int
}

func NewInt(s string) *Int {
	var val int64
	var base int
	var err error
	var missed string

	if strings.HasPrefix(s, "0x") || strings.HasPrefix(s, "0X") {
		missed = s[:2]
		s = s[2:]
		base = 16
	} else {
		base = 10
	}
	val, err = strconv.ParseInt(s, base, 64)
	if err != nil {
		panic(fmt.Sprintf("%s is not integer format", missed+s))
	}
	return &Int{Value: val, Base: base}
}

func (self *Int) Eval(env *scope.Scope) value.Value {
	return value.NewIntValue(self.Value)
}

func (self *Int) String() string {
	return strconv.FormatInt(self.Value, self.Base)
}
