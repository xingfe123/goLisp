package ast

import (
	"fmt"
	"xingfe91/LispEx/scope"
	"xingfe91/LispEx/value"
)

type Tuple struct {
	Elements []Node
}

func NewTuple(elements []Node) *Tuple {
	return &Tuple{Elements: elements}
}

func (self *Tuple) Eval(env *scope.Scope) value.Value {
	panic("unsupported tuple evaluation")
	return nil
}

func (self *Tuple) String() string {
	var s string
	for i, e := range self.Elements {
		if i == 0 {
			s += e.String()
		} else {
			s += fmt.Sprintf(" %s", e)
		}
	}
	return fmt.Sprintf("(%s)", s)
}

///////////////////////////////////////////////////////
type UnquoteBase struct {
	Body   Node
	Prompt string
}

//////////////////////////////////////////////////////////////is same//
func NewUnquote(body Node) *UnquoteBase {
	return &UnquoteBase{Body: body,
		Prompt: ",%s"}
}
func NewUnquoteSplicing(body Node) *UnquoteBase {
	return &UnquoteBase{Body: body,
		Prompt: ",@%s"}
}
func (self *UnquoteBase) Eval(env *scope.Scope) value.Value {
	return self.Body.Eval(env)
}

func (self *UnquoteBase) String() string {
	return fmt.Sprintf(self.Prompt, self.Body)
}

/////////////////////////////////////////////////////////////////////////////
type QuoteBase struct {
	Body   Node
	Prompt string
}

func NewQuote(body Node) *QuoteBase {
	return &QuoteBase{Body: body,
		Prompt: "'%s"}
}
func NewQuasiquote(body Node) *QuoteBase {
	return &QuoteBase{Body: body,
		Prompt: "`%s"}
}
func (self *QuoteBase) Eval(env *scope.Scope) value.Value {
	if name, ok := self.Body.(*Name); ok {
		return value.NewSymbol(name.Identifier)
	} else {
		return self.Body.Eval(env)
	}
}

func (self *QuoteBase) String() string {
	return fmt.Sprintf(self.Prompt, self.Body)
}

/////////////////////////////////////////////////////////////////
