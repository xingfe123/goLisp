package ast

import (
	"fmt"
	"xingfe91/LispEx/converter"
	"xingfe91/LispEx/scope"
	"xingfe91/LispEx/value"
)

type Apply struct {
	Proc Node
	Args []Node
}

func NewApply(proc Node, args []Node) *Apply {
	return &Apply{Proc: proc, Args: args}
}

func (self *Apply) Eval(s *scope.Scope) value.Value {
	// (apply proc arg1 xingfe91/LispEx. args)
	// Proc must be a procedure and args must be a list.
	// Calls proc with the elements of the list
	// (append (list arg1 xingfe91/LispEx.) args) as the actual arguments.

	proc := self.Proc.Eval(s)
	args := ExpandApplyArgs(EvalList(self.Args, s))

	switch proc.(type) {
	case *value.Closure:
		curry := proc.(*value.Closure)
		lambda, ok := curry.Body.(*Lambda)
		if !ok {
			panic(fmt.Sprint("unexpected type: ", curry.Body))
		}
		env := curry.Env.(*scope.Scope)
		BindArguments(env, lambda.Params, args)
		return lambda.Body.Eval(env)
	case value.PrimFunc:
		return proc.(value.PrimFunc).Apply(converter.PairsToSlice(args))
	default:
		panic(fmt.Sprintf("apply: expected a procedure, given: %s", self.Proc))
	}
}

func (self *Apply) String() string {
	return fmt.Sprintf("(apply %s %s)", self.Proc, self.Args)
}

// (1 2 '(3)) => (1 2 3)
func ExpandApplyArgs(args []value.Value) value.Value {
	prev := value.NewPairValue(nil, nil)
	curr := value.NewPairValue(nil, nil)
	front := prev
	expectlist := false

	for i, arg := range args {
		switch arg.(type) {
		case *value.PairValue:
			prev.Second = arg.(*value.PairValue)
			for {
				if _, ok := arg.(*value.PairValue); ok {
					arg = arg.(*value.PairValue).Second
				} else if _, ok := arg.(*value.EmptyPairValue); ok {
					break
				} else {
					panic(fmt.Sprint("apply: expected list, given: ", arg))
				}
			}
			expectlist = false
			if i != len(args)-1 {
				panic(fmt.Sprint("apply: expected list, given: ", arg))
			}
		case *value.EmptyPairValue:
			expectlist = false
			if i != len(args)-1 {
				panic(fmt.Sprint("apply: expected list, given: ", arg))
			}
		default:
			expectlist = true
			curr.First = arg
			prev.Second = curr
			prev = curr
			curr = value.NewPairValue(nil, nil)
		}
	}
	if expectlist {
		panic(fmt.Sprint("apply: expected list, given: ", args[len(args)-1]))
	}
	return front.Second
}
