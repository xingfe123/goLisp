package value

import "fmt"
import "strconv"

type Value interface {
	String() string
}

////////////////////////////////////////////////////////
type Channel struct {
	Value chan Value
}

func NewChannel(size int) *Channel {
	return &Channel{Value: make(chan Value, size)}
}

func (self *Channel) String() string {
	return fmt.Sprint(self.Value)
}

/////////////////////////////////////////////////////////////////////////
type BoolValue struct {
	Value bool
}

func NewBoolValue(val bool) *BoolValue {
	return &BoolValue{Value: val}
}

func (self *BoolValue) String() string {
	if self.Value {
		return "#t"
	} else {
		return "#f"
	}
}

///////////////////////////////////////////////////////////////////////////
type Closure struct {
	Env  interface{}
	Body interface{}
}

func NewClosure(env, body interface{}) *Closure {
	return &Closure{Env: env, Body: body}
}

func (self *Closure) String() string {
	return "#<procedure>"
}

///////////////////////////////////
type IntValue struct {
	Value int64
}

func NewIntValue(val int64) *IntValue {
	return &IntValue{Value: val}
}

func (v *IntValue) String() string {
	return strconv.FormatInt(v.Value, 10)
}

type FloatValue struct {
	Value float64
}

func NewFloatValue(val float64) *FloatValue {
	return &FloatValue{Value: val}
}

func (self *FloatValue) String() string {
	return strconv.FormatFloat(self.Value, 'f', -1, 64)
}

/////////////////
type PrimFunc interface {
	Value
	Apply(args []Value) Value
}

type Primitive struct {
	Name string
}

func (self *Primitive) String() string {
	return self.Name
}

type Promise struct {
	IsVal bool
	Env   interface{}
	Lazy  interface{}
}

func NewPromise(env, lazy interface{}) *Promise {
	return &Promise{IsVal: true, Env: env, Lazy: lazy}
}

func (self *Promise) String() string {
	return "#<promise>"
}

type StringValue struct {
	Value string
}

func NewStringValue(val string) *StringValue {
	return &StringValue{Value: val}
}

func (self *StringValue) String() string {
	return fmt.Sprintf("\"%s\"", self.Value)
}

type Symbol struct {
	Value string
}

func NewSymbol(value string) *Symbol {
	return &Symbol{Value: value}
}

func (self *Symbol) String() string {
	return self.Value
}
