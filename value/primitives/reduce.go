package primitives

import (
	"fmt"
	"xingfe91/LispEx/value"
)

type And struct {
	value.Primitive
}

func NewAnd() *And {
	return &And{value.Primitive{"and"}}
}
func (self *And) Apply(args []value.Value) value.Value {
	result := true
	for _, arg := range args {
		if val, ok := arg.(*value.BoolValue); ok {
			result = result && val.Value
		} else {
			panic(fmt.Sprint(
				"incorrect argument type for `and', expected bool?"))
		}
	}
	return value.NewBoolValue(result)
}

type Or struct {
	value.Primitive
}

func NewOr() *Or {
	return &Or{value.Primitive{"or"}}
}

func (self *Or) Apply(args []value.Value) value.Value {
	result := false
	for _, arg := range args {
		if val, ok := arg.(*value.BoolValue); ok {
			result = result || val.Value
		} else {
			panic(fmt.Sprint("incorrect argument type for `or', expected bool?"))
		}
	}
	return value.NewBoolValue(result)
}
