package primitives

import (
	"fmt"
	"xingfe91/LispEx/value"
)

func checkargsnumber(args []value.Value, n int) {
	if len(args) != n {
		panic(fmt.Sprintf("cons: arguments mismatch, expected: %i, given: ",
			n,
			args))
	}
}

type Cons struct {
	value.Primitive
}

func NewCons() *Cons {
	return &Cons{value.Primitive{"cons"}}
}

func (self *Cons) Apply(args []value.Value) value.Value {
	checkargsnumber(args, 2)
	return value.NewPairValue(args[0], args[1])
}

type Car struct {
	value.Primitive
}

func NewCar() *Car {
	return &Car{value.Primitive{"car"}}
}

func (self *Car) Apply(args []value.Value) value.Value {
	checkargsnumber(args, 1)
	pairs := args[0]
	switch pairs.(type) {
	case *value.PairValue:
		return pairs.(*value.PairValue).First
	default:
		panic(fmt.Sprint("car: expected pair, given: ", pairs))
	}
}

type Cdr struct {
	value.Primitive
}

func NewCdr() *Cdr {
	return &Cdr{value.Primitive{"cdr"}}
}

func (self *Cdr) Apply(args []value.Value) value.Value {
	checkargsnumber(args, 1)
	pairs := args[0]
	switch pairs.(type) {
	case *value.PairValue:
		return pairs.(*value.PairValue).Second
	default:
		panic(fmt.Sprint("cdr: expected pair, given: ", pairs))
	}
}

type Eq struct {
	value.Primitive
}

func NewEq() *Eq {
	return &Eq{value.Primitive{"="}}
}

func (self *Eq) Apply(args []value.Value) value.Value {
	checkargsnumber(args, 2)
	if v1, ok := args[0].(*value.IntValue); ok {
		if v2, ok := args[1].(*value.IntValue); ok {
			return value.NewBoolValue(v1.Value == v2.Value)
		} else if v2, ok := args[1].(*value.FloatValue); ok {
			return value.NewBoolValue(float64(v1.Value) == v2.Value)
		}
	} else if v1, ok := args[0].(*value.FloatValue); ok {
		if v2, ok := args[1].(*value.IntValue); ok {
			return value.NewBoolValue(v1.Value == float64(v2.Value))
		} else if v2, ok := args[1].(*value.FloatValue); ok {
			return value.NewBoolValue(v1.Value == v2.Value)
		}
	}
	panic(fmt.Sprint("incorrect argument type for `=', expected number?"))
}
