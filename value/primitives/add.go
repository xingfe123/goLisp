package primitives

import (
	"fmt"
	"xingfe91/LispEx/value"
)

type Add struct {
	value.Primitive
}

func NewAdd() *Add {
	return &Add{value.Primitive{"+"}}
}

func (self *Add) Apply(args []value.Value) value.Value {
	var val1 int64
	var val2 float64
	isfloat := false

	for _, arg := range args {
		switch arg.(type) {
		case *value.IntValue:
			val1 += arg.(*value.IntValue).Value
		case *value.FloatValue:
			isfloat = true
			val2 += arg.(*value.FloatValue).Value
		default:
			panic(fmt.Sprint("incorrect argument type for '+' : ", arg))
		}
	}
	if !isfloat {
		return value.NewIntValue(val1)
	} else {
		return value.NewFloatValue(float64(val1) + val2)
	}
}

type Mod struct {
	value.Primitive
}

func NewMod() *Mod {
	return &Mod{value.Primitive{"%"}}
}

func (self *Mod) Apply(args []value.Value) value.Value {
	if len(args) != 2 {
		panic(fmt.Sprint("argument mismatch for `%', expected 2, given: ", len(args)))
	}
	if v1, ok := args[0].(*value.IntValue); ok {
		if v2, ok := args[1].(*value.IntValue); ok {
			if v2.Value == 0 {
				panic(fmt.Sprint("remainder: undefined for 0"))
			}
			return value.NewIntValue(v1.Value % v2.Value)
		}
	}
	panic(fmt.Sprint("incorrect argument type for `%', expected integer?"))
}

type Mult struct {
	value.Primitive
}

func NewMult() *Mult {
	return &Mult{value.Primitive{"*"}}
}

func (self *Mult) Apply(args []value.Value) value.Value {
	var val1 int64 = 1
	var val2 float64 = 1
	isfloat := false

	for _, arg := range args {
		switch arg.(type) {
		case *value.IntValue:
			val1 *= arg.(*value.IntValue).Value
		case *value.FloatValue:
			isfloat = true
			val2 *= arg.(*value.FloatValue).Value
		default:
			panic(fmt.Sprint("incorrect argument type for '*' : ", arg))
		}
	}
	if !isfloat {
		return value.NewIntValue(val1)
	} else {
		return value.NewFloatValue(float64(val1) * val2)
	}
}

type Sub struct {
	value.Primitive
}

func NewSub() *Sub {
	return &Sub{value.Primitive{"-"}}
}

func (self *Sub) Apply(args []value.Value) value.Value {
	var val float64
	isfloat := false

	if len(args) == 0 {
		panic(fmt.Sprint("`-' argument unmatch: expected at least 1"))
	} else if len(args) > 1 {
		switch args[0].(type) {
		case *value.IntValue:
			val = float64(args[0].(*value.IntValue).Value)
		case *value.FloatValue:
			val = args[0].(*value.FloatValue).Value
		default:
			panic(fmt.Sprint("incorrect argument type for `-' : ", args[0]))
		}
		args = args[1:]
	}

	for _, arg := range args {
		switch arg.(type) {
		case *value.IntValue:
			val -= float64(arg.(*value.IntValue).Value)
		case *value.FloatValue:
			isfloat = true
			val -= arg.(*value.FloatValue).Value
		default:
			panic(fmt.Sprint("incorrect argument type for `-' : ", arg))
		}

	}
	if isfloat {
		return value.NewFloatValue(val)
	} else {
		return value.NewIntValue(int64(val))
	}
}

type Div struct {
	value.Primitive
}

func NewDiv() *Div {
	return &Div{value.Primitive{"/"}}
}

func (self *Div) Apply(args []value.Value) value.Value {
	var val float64 = 1

	if len(args) == 0 {
		panic(fmt.Sprint("`/' argument unmatch: expected at least 1"))
	} else if len(args) > 1 {
		switch args[0].(type) {
		case *value.IntValue:
			val = float64(args[0].(*value.IntValue).Value)
		case *value.FloatValue:
			val = args[0].(*value.FloatValue).Value
		default:
			panic(fmt.Sprint("incorrect argument type for `/' : ", args[0]))
		}
		args = args[1:]
	}
	if len(args) == 0 && val == 0 {
		// (/ 0)
		panic(fmt.Sprint("`/' division by zero"))
	}

	for _, arg := range args {
		switch arg.(type) {
		case *value.IntValue:
			divisor := arg.(*value.IntValue).Value
			if divisor == 0 {
				panic(fmt.Sprint("`/' division by zero"))
			}
			val /= float64(divisor)
		case *value.FloatValue:
			divisor := arg.(*value.FloatValue).Value
			if divisor == 0 {
				panic(fmt.Sprint("`/' division by zero"))
			}
			val /= divisor
		default:
			panic(fmt.Sprint("incorrect argument type for `/' : ", arg))
		}

	}
	return value.NewFloatValue(val)
}
